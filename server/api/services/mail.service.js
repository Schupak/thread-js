import nodemailer from "nodemailer";

export const sendEmail = (to, subject, text) =>
  new Promise(resolve => {
    // Using https://ethereal.email testing service recommended by nodemailer, which generates accounts
    const transporter = nodemailer.createTransport({
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user: "enoch46@ethereal.email",
        pass: "F873XyjW7DJ919Gq3Y"
      }
    });

    const mailOptions = {
      from: "zita.turcotte@ethereal.email",
      to,
      subject,
      text
    };

    transporter.sendMail(mailOptions, err => {
      if (err) {
        console.error("there was an error: ", err);
      } else {
        resolve("OK");
      }
    });
  });

export const sharePostByEmail = (recipient, postId, sender) => {
  const text = `User ${sender} shared the post with you!\n\nCheck it out: http://localhost:3001/share/${postId}`;
  return sendEmail(recipient, "A Post Was Shared With You", text);
};

export const sendNotificationEmail = (email, reactedUsername, postId) => {
  // Using https://ethereal.email testing service recommended by nodemailer, which generates accounts
  const transporter = nodemailer.createTransport({
    host: "smtp.ethereal.email",
    port: 587,
    auth: {
      user: "zita.turcotte@ethereal.email",
      pass: "J5KS1DKcbH5d8yXbqF"
    }
  });

  const mailOptions = {
    from: "zita.turcotte@ethereal.email",
    to: email,
    subject: "Your Post Was Liked",
    text: `Your post (http://localhost:3001/share/${postId}) was recently liked by ${reactedUsername}!`
  };

  transporter.sendMail(mailOptions);
};
