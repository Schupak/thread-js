import React from "react";
import PropTypes from "prop-types";
import { Comment as CommentUI, Input, Button, Icon } from "semantic-ui-react";
import moment from "moment";
import { getUserImgLink } from "src/helpers/imageHelper";

import styles from "./styles.module.scss";

class Comment extends React.Component {
  constructor(props) {
    super(props);
    this.state = { editMode: false };
    this.commentId = this.props.comment.id;
    this.commentBody = this.props.comment.body;

    this.toggleCommentMode = this.toggleCommentMode.bind(this);
    this.handleUpdateComment = this.handleUpdateComment.bind(this);
  }

  toggleCommentMode = () => {
    this.setState(state => ({ editMode: !state.editMode }));
  };

  handleUpdateComment = updatedCommentBody => {
    if (!updatedCommentBody || this.commentBody === updatedCommentBody) {
      this.toggleCommentMode();
      return;
    }

    this.props.updateComment({ body: updatedCommentBody }, this.commentId);
    this.toggleCommentMode();
  };

  render() {
    const { comment, userStatus, likeComment } = this.props;
    const { id, body, createdAt, updatedAt, user } = comment;
    let date;
    if (new Date(comment.updatedAt) <= new Date(comment.createdAt)) {
      date = moment(createdAt).fromNow();
      date = `posted ${date}`;
    } else {
      date = moment(updatedAt).fromNow();
      date = `last updated ${date}`;
    }
    let updatedText;
    let content;
    if (this.state.editMode) {
      content = (
        <Input
          action
          placeholder="Enter comment text"
          defaultValue={body}
          onChange={(event, data) => {
            updatedText = data.value;
          }}
        >
          <input />
          <Button color="red" onClick={() => this.toggleCommentMode()}>
            Cancel
          </Button>
          <Button
            color="blue"
            onClick={() => this.handleUpdateComment(updatedText)}
          >
            Save
          </Button>
        </Input>
      );
    } else {
      content = (
        <>
          {this.props.isCurrentUserComment ? (
            <div>
              <CommentUI.Author as="a">{user.username}</CommentUI.Author>
              <CommentUI.Metadata>{date}</CommentUI.Metadata>
              <CommentUI.Text>
                <div class="ui pointing label">{userStatus}</div>
              </CommentUI.Text>
              <CommentUI.Text>{body}</CommentUI.Text>
            </div>
          ) : (
            <div>
              <CommentUI.Author as="a">{user.username}</CommentUI.Author>
              <CommentUI.Metadata>{date}</CommentUI.Metadata>
              <CommentUI.Text>{body}</CommentUI.Text>
            </div>
          )}

          {this.props.isCurrentUserComment ? (
            <div>
              <Icon
                link
                color="black"
                name="edit"
                className={styles.editBtn}
                onClick={() => this.toggleCommentMode()}
              />
              <Button
                onClick={() => this.props.deleteComment(this.props.comment)}
              >
                Delete
              </Button>
              {/* <CommentUI.Actions>
                <CommentUI.Action onClick={() => likeComment(id)}>
                  <Icon name="thumbs up" />
                  {this.props.comment.likeCount}
                </CommentUI.Action>
              </CommentUI.Actions> */}
            </div>
          ) : null}
        </>
      );
    }
    return (
      <CommentUI className={styles.comment}>
        <CommentUI.Avatar src={getUserImgLink(user.image)} />
        <CommentUI.Content>{content}</CommentUI.Content>
      </CommentUI>
    );
  }
}

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  isCurrentUserComment: PropTypes.bool.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

export default Comment;
