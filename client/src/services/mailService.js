import callWebApi from 'src/helpers/webApiHelper';

export const sharePostByEmail = async request => callWebApi({
    endpoint: '/api/mailer/sharePost',
    type: 'POST',
    request
});
