import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getUserImgLink } from "src/helpers/imageHelper";
import { bindActionCreators } from "redux";
import { Button, Grid, Image, Input } from "semantic-ui-react";
import { updateUser } from "./actions";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      updatedUsername: this.props.user.username,
      updatedStatus: this.props.user.status,
      editPhoto: false,
      editUsername: false,
      editStatus: false
    };

    this.toggleEditUsername = this.toggleEditUsername.bind(this);
    this.handleEditUsername = this.handleEditUsername.bind(this);
    this.handleUpdateUsernameValue = this.handleUpdateUsernameValue.bind(this);
    this.toggleEditStatus = this.toggleEditStatus.bind(this);
    this.handleEditStatus = this.handleEditStatus.bind(this);
    this.handleUpdateStatusValue = this.handleUpdateStatusValue.bind(this);
  }

  toggleEditStatus(event) {
    if (
      event &&
      event.relatedTarget &&
      event.relatedTarget.tagName === "BUTTON"
    )
      return;

    this.setState(({ editStatus }) => ({
      editStatus: !editStatus
    }));
  }

  toggleEditUsername(event) {
    this.setState(({ editUsername }) => ({
      editUsername: !editUsername
    }));
  }

  handleUpdateUsernameValue(updatedUsername) {
    this.setState({ updatedUsername });
  }

  handleUpdateStatusValue(updatedStatus) {
    this.setState({ updatedStatus });
  }

  handleEditStatus() {
    const { updatedStatus } = this.state;
    if (!updatedStatus || this.props.user.status === updatedStatus) {
      this.toggleEditStatus();
      return;
    }

    this.props.updateUser({ ...this.props.user, status: updatedStatus });
    this.toggleEditStatus();
  }

  handleEditUsername() {
    const { updatedUsername, editStatus } = this.state;
    if (!updatedUsername || this.props.user.username === updatedUsername) {
      this.toggleEditUsername();
      return;
    }

    this.props.updateUser({ ...this.props.user, username: updatedUsername });
    this.toggleEditUsername();
  }

  render() {
    const { user } = this.props;
    const { editUsername, editStatus } = this.state;

    return (
      <Grid container textAlign="center" style={{ paddingTop: 30 }}>
        <Grid.Column>
          <Image
            centered
            src={getUserImgLink(user.image)}
            size="medium"
            circular
          />
                    <Input
            action={
              editStatus ? (
                <>
                  <Button
                    color="red"
                    onClick={event => this.toggleEditStatus(event)}
                  >
                    Cancel
                  </Button>
                  <Button color="blue" onClick={this.handleEditStatus}>
                    Save
                  </Button>
                </>
              ) : null
            }
            icon="info"
            iconPosition="left"
            placeholder="Status"
            type="text"
            onFocus={this.toggleEditStatus}
            onBlur={event => this.toggleEditStatus(event)}
            onChange={(event, data) => {
              this.handleUpdateStatusValue(data.value);
            }}
            defaultValue={user.status}
          />
          <br />
          <br/>
          <Input
            action={
              editUsername ? (
                <>
                  <Button
                    color="red"
                    onClick={event => this.toggleEditUsername(event)}
                  >
                    Cancel
                  </Button>
                  <Button
                    color="blue"
                    onClick={() => this.handleEditUsername()}
                  >
                    Save
                  </Button>
                </>
              ) : null
            }
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            onFocus={this.toggleEditUsername}
            onBlur={event => this.toggleEditUsername(event)}
            onChange={(event, data) => {
              this.handleUpdateUsernameValue(data.value);
            }}
            defaultValue={user.username}
          />
          <br />
          <br />
          <Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            disabled
            value={user.email}
          />
          <br />
          <br />
        </Grid.Column>
      </Grid>
    );
  }
}

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUser: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = { updateUser };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
